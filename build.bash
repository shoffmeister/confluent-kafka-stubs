#!/usr/bin/env bash

set -euo pipefail

echo "Validating type stubs using pyright:"
pyright .

echo "Validating type stubs using mypy:"
mypy .

poetry build
