# Copyright 2020 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from .cimpl import *  # noqa: F401,F403

class ThrottleEvent(object):
    """
    ThrottleEvent contains details about a throttled request.
    Set up a throttle callback by setting the ``throttle_cb`` configuration
    property to a callable that takes a ThrottleEvent object as its only argument.
    The callback will be triggered from poll(), consume() or flush() when a request
    has been throttled by the broker.

    This class is typically not user instantiated.

    :ivar str broker_name: The hostname of the broker which throttled the request
    :ivar int broker_id: The broker id
    :ivar float throttle_time: The amount of time (in seconds) the broker throttled (delayed) the request
    """
    broker_name: str
    """The hostname of the broker which throttled the request"""
    broker_id: int
    """The broker id"""
    throttle_time: float
    """The amount of time (in seconds) the broker throttled (delayed) the request"""

    def __init__(self, broker_name: str,
                 broker_id: int,
                 throttle_time: float) -> None: ...
