# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from enum import Enum

from confluent_kafka.cimpl import TopicPartition

class Node:
    """
    Represents node information.
    Used by :class:`ConsumerGroupDescription`

    Parameters
    ----------
    id: int
        The node id of this node.
    id_string:
        String representation of the node id.
    host:
        The host name for this node.
    port: int
        The port for this node.
    rack: str
        The rack for this node.
    """

    id: int
    id_string: str
    host: str
    port: int
    rack: str

class ConsumerGroupTopicPartitions:
    """
    Represents consumer group and its topic partition information.
    Used by :meth:`AdminClient.list_consumer_group_offsets` and
    :meth:`AdminClient.alter_consumer_group_offsets`.

    Parameters
    ----------
    group_id: str
        Id of the consumer group.
    topic_partitions: list(TopicPartition)
        List of topic partitions information.
    """

    group_id: str
    topic_partitions: list[TopicPartition]

class ConsumerGroupState(Enum):
    """
    Enumerates the different types of Consumer Group State.

    Note that the state :py:attr:`UNKOWN` (typo one) is deprecated and will be removed in
    future major release. Use :py:attr:`UNKNOWN` instead.
    """

    #: State is not known or not set
    UNKNOWN: int
    #: Preparing rebalance for the consumer group.
    PREPARING_REBALANCING: int
    #: Consumer Group is completing rebalancing.
    COMPLETING_REBALANCING: int
    #: Consumer Group is stable.
    STABLE: int
    #: Consumer Group is dead.
    DEAD: int
    #: Consumer Group is empty.
    EMPTY: int

class TopicCollection:
    """
    Represents collection of topics in the form of different identifiers
    for the topic.

    Parameters
    ----------
    topic_names: list(str)
        List of topic names.
    """

    topic_names: list[str]

class TopicPartitionInfo:
    """
    Represents partition information.
    Used by :class:`TopicDescription`.

    Parameters
    ----------
    id : int
        Id of the partition.
    leader : Node
        Leader broker for the partition.
    replicas: list(Node)
        Replica brokers for the partition.
    isr: list(Node)
        In-Sync-Replica brokers for the partition.
    """

    id: int
    leader: Node
    replicas: list[Node]
    isr: list[Node]

class IsolationLevel(Enum):
    """
    Enum for Kafka isolation levels.

    Values:
    -------
    """

    READ_UNCOMMITTED: int
    READ_COMMITTED: int
