#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2020 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any, Callable, Optional, Union

from confluent_kafka.serialization import Deserializer, Serializer, SerializationContext
from confluent_kafka.schema_registry import topic_subject_name_strategy
from confluent_kafka.schema_registry.schema_registry_client import (
    Schema,
    SchemaRegistryClient,
)

class AvroSerializer(Serializer):
    """
    Serializer that outputs Avro binary encoded data with Confluent Schema Registry framing.

    Configuration properties:

    +---------------------------+----------+--------------------------------------------------+
    | Property Name             | Type     | Description                                      |
    +===========================+==========+==================================================+
    |                           |          | If True, automatically register the configured   |
    | ``auto.register.schemas`` | bool     | schema with Confluent Schema Registry if it has  |
    |                           |          | not previously been associated with the relevant |
    |                           |          | subject (determined via subject.name.strategy).  |
    |                           |          |                                                  |
    |                           |          | Defaults to True.                                |
    +---------------------------+----------+--------------------------------------------------+
    |                           |          | Whether to normalize schemas, which will         |
    | ``normalize.schemas``     | bool     | transform schemas to have a consistent format,   |
    |                           |          | including ordering properties and references.    |
    +---------------------------+----------+--------------------------------------------------+
    |                           |          | Whether to use the latest subject version for    |
    | ``use.latest.version``    | bool     | serialization.                                   |
    |                           |          |                                                  |
    |                           |          | WARNING: There is no check that the latest       |
    |                           |          | schema is backwards compatible with the object   |
    |                           |          | being serialized.                                |
    |                           |          |                                                  |
    |                           |          | Defaults to False.                               |
    +---------------------------+----------+--------------------------------------------------+
    |                           |          | Callable(SerializationContext, str) -> str       |
    |                           |          |                                                  |
    | ``subject.name.strategy`` | callable | Defines how Schema Registry subject names are    |
    |                           |          | constructed. Standard naming strategies are      |
    |                           |          | defined in the confluent_kafka.schema_registry   |
    |                           |          | namespace.                                       |
    |                           |          |                                                  |
    |                           |          | Defaults to topic_subject_name_strategy.         |
    +---------------------------+----------+--------------------------------------------------+

    Schemas are registered against subject names in Confluent Schema Registry that
    define a scope in which the schemas can be evolved. By default, the subject name
    is formed by concatenating the topic name with the message field (key or value)
    separated by a hyphen.

    i.e. {topic name}-{message field}

    Alternative naming strategies may be configured with the property
    ``subject.name.strategy``.

    Supported subject name strategies:

    +--------------------------------------+------------------------------+
    | Subject Name Strategy                | Output Format                |
    +======================================+==============================+
    | topic_subject_name_strategy(default) | {topic name}-{message field} |
    +--------------------------------------+------------------------------+
    | topic_record_subject_name_strategy   | {topic name}-{record name}   |
    +--------------------------------------+------------------------------+
    | record_subject_name_strategy         | {record name}                |
    +--------------------------------------+------------------------------+

    See `Subject name strategy <https://docs.confluent.io/current/schema-registry/serializer-formatter.html#subject-name-strategy>`_ for additional details.

    Note:
        Prior to serialization, all values must first be converted to
        a dict instance. This may handled manually prior to calling
        :py:func:`Producer.produce()` or by registering a `to_dict`
        callable with AvroSerializer.

        See ``avro_producer.py`` in the examples directory for example usage.

    Note:
       Tuple notation can be used to determine which branch of an ambiguous union to take.

       See `fastavro notation <https://fastavro.readthedocs.io/en/latest/writer.html#using-the-tuple-notation-to-specify-which-branch-of-a-union-to-take>`_

    Args:
        schema_registry_client (SchemaRegistryClient): Schema Registry client instance.

        schema_str (str or Schema):
            Avro `Schema Declaration. <https://avro.apache.org/docs/current/spec.html#schemas>`_
            Accepts either a string or a :py:class:`Schema` instance. Note that string
            definitions cannot reference other schemas. For referencing other schemas,
            use a :py:class:`Schema` instance.

        to_dict (callable, optional): Callable(object, SerializationContext) -> dict. Converts object to a dict.

        conf (dict): AvroSerializer configuration.
    """  # noqa: E501

    _default_conf = {
        "auto.register.schemas": True,
        "normalize.schemas": False,
        "use.latest.version": False,
        "subject.name.strategy": topic_subject_name_strategy,
    }

    def __init__(
        self,
        schema_registry_client: SchemaRegistryClient,
        schema_str: Union[str, Schema],
        to_dict: Optional[
            Callable[[object, SerializationContext], dict[Any, Any]]
        ] = None,
        conf: Optional[dict[Any, Any]] = None,
    ) -> None: ...
    def __call__(
        self, obj: Optional[object], ctx: Optional[SerializationContext]
    ) -> Optional[bytes]: ...
    """
    Serializes an object to Avro binary format, prepending it with Confluent
    Schema Registry framing.

    Args:
        obj (object): The object instance to serialize.

        ctx (SerializationContext): Metadata pertaining to the serialization operation.

    Raises:
        SerializerError: If any error occurs serializing obj.
        SchemaRegistryError: If there was an error registering the schema with
                                Schema Registry, or auto.register.schemas is
                                false and the schema was not registered.

    Returns:
        bytes: Confluent Schema Registry encoded Avro bytes
    """

class AvroDeserializer(Deserializer):
    """
    Deserializer for Avro binary encoded data with Confluent Schema Registry
    framing.

    Note:
        By default, Avro complex types are returned as dicts. This behavior can
        be overriden by registering a callable ``from_dict`` with the deserializer to
        convert the dicts to the desired type.

        See ``avro_consumer.py`` in the examples directory in the examples
        directory for example usage.

    Args:
        schema_registry_client (SchemaRegistryClient): Confluent Schema Registry
            client instance.

        schema_str (str, Schema, optional): Avro reader schema declaration Accepts
            either a string or a :py:class:`Schema` instance. If not provided, the
            writer schema will be used as the reader schema. Note that string
            definitions cannot reference other schemas. For referencing other schemas,
            use a :py:class:`Schema` instance.

        from_dict (callable, optional): Callable(dict, SerializationContext) -> object.
            Converts a dict to an instance of some object.

        return_record_name (bool): If True, when reading a union of records, the result will
                                   be a tuple where the first value is the name of the record and the second value is
                                   the record itself.  Defaults to False.

    See Also:
        `Apache Avro Schema Declaration <https://avro.apache.org/docs/current/spec.html#schemas>`_

        `Apache Avro Schema Resolution <https://avro.apache.org/docs/1.8.2/spec.html#Schema+Resolution>`_
    """

    def __init__(
        self,
        schema_registry_client: SchemaRegistryClient,
        schema_str: Optional[Union[str, Schema]],
        from_dict: Optional[Callable[[dict[Any, Any]], object]] = None,
        return_record_name: bool = False,
    ) -> None: ...
    def __call__(
        self, data: Optional[bytes], ctx: Optional[SerializationContext]
    ) -> Optional[Any]: ...
    """
    Deserialize Avro binary encoded data with Confluent Schema Registry framing to
    a dict, or object instance according to from_dict, if specified.

    Arguments:
        data (bytes): bytes

        ctx (SerializationContext): Metadata relevant to the serialization
            operation.

    Raises:
        SerializerError: if an error occurs parsing data.

    Returns:
        object: If data is None, then None. Else, a dict, or object instance according
                to from_dict, if specified.
    """  # noqa: E501
