#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2020 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


from typing import Literal, Optional, Union


VALID_AUTH_PROVIDERS = ['URL', 'USER_INFO']


class SchemaRegistryClient(object):
    """
    A Confluent Schema Registry client.

    Configuration properties (* indicates a required field):

    +------------------------------+------+-------------------------------------------------+
    | Property name                | type | Description                                     |
    +==============================+======+=================================================+
    | ``url`` *                    | str  | Schema Registry URL.                            |
    +------------------------------+------+-------------------------------------------------+
    |                              |      | Path to CA certificate file used                |
    | ``ssl.ca.location``          | str  | to verify the Schema Registry's                 |
    |                              |      | private key.                                    |
    +------------------------------+------+-------------------------------------------------+
    |                              |      | Path to client's private key                    |
    |                              |      | (PEM) used for authentication.                  |
    | ``ssl.key.location``         | str  |                                                 |
    |                              |      | ``ssl.certificate.location`` must also be set.  |
    +------------------------------+------+-------------------------------------------------+
    |                              |      | Path to client's public key (PEM) used for      |
    |                              |      | authentication.                                 |
    | ``ssl.certificate.location`` | str  |                                                 |
    |                              |      | May be set without ssl.key.location if the      |
    |                              |      | private key is stored within the PEM as well.   |
    +------------------------------+------+-------------------------------------------------+
    |                              |      | Client HTTP credentials in the form of          |
    |                              |      | ``username:password``.                          |
    | ``basic.auth.user.info``     | str  |                                                 |
    |                              |      | By default userinfo is extracted from           |
    |                              |      | the URL if present.                             |
    +------------------------------+------+-------------------------------------------------+

    Args:
        conf (dict): Schema Registry client configuration.

    See Also:
        `Confluent Schema Registry documentation <http://confluent.io/docs/current/schema-registry/docs/intro.html>`_
    """  # noqa: E501

    def __init__(self, conf: dict[str, str]) -> None: ...

    def register_schema(self, subject_name: str, schema: Schema, normalize_schemas: bool=False) -> int: ...
    """
    Registers a schema under ``subject_name``.

    Args:
        subject_name (str): subject to register a schema under

        schema (Schema): Schema instance to register

    Returns:
        int: Schema id

    Raises:
        SchemaRegistryError: if Schema violates this subject's
            Compatibility policy or is otherwise invalid.

    See Also:
        `POST Subject API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#post--subjects-(string-%20subject)-versions>`_
    """  # noqa: E501


    def get_schema(self, schema_id: int) -> Schema: ...
    """
    Fetches the schema associated with ``schema_id`` from the
    Schema Registry. The result is cached so subsequent attempts will not
    require an additional round-trip to the Schema Registry.

    Args:
        schema_id (int): Schema id

    Returns:
        Schema: Schema instance identified by the ``schema_id``

    Raises:
        SchemaRegistryError: If schema can't be found.

    See Also:
        `GET Schema API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#get--schemas-ids-int-%20id>`_
    """  # noqa: E501


    def lookup_schema(self, subject_name: str, schema: Schema, normalize_schemas: bool=False) -> RegisteredSchema: ...
    """
    Returns ``schema`` registration information for ``subject``.

    Args:
        subject_name (str): Subject name the schema is registered under

        schema (Schema): Schema instance.

    Returns:
        RegisteredSchema: Subject registration information for this schema.

    Raises:
        SchemaRegistryError: If schema or subject can't be found

    See Also:
        `POST Subject API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#post--subjects-(string-%20subject)-versions>`_
    """  # noqa: E501


    def get_subjects(self) -> list[str]: ...
    """
    List all subjects registered with the Schema Registry

    Returns:
        list(str): Registered subject names

    Raises:
        SchemaRegistryError: if subjects can't be found

    See Also:
        `GET subjects API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#get--subjects-(string-%20subject)-versions>`_
    """  # noqa: E501


    def delete_subject(self, subject_name: str, permanent: bool=False) -> list[int]: ...
    """
    Deletes the specified subject and its associated compatibility level if
    registered. It is recommended to use this API only when a topic needs
    to be recycled or in development environments.

    Args:
        subject_name (str): subject name
        permanent (bool): True for a hard delete, False (default) for a soft delete

    Returns:
        list(int): Versions deleted under this subject

    Raises:
        SchemaRegistryError: if the request was unsuccessful.

    See Also:
        `DELETE Subject API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#delete--subjects-(string-%20subject)>`_
    """  # noqa: E501


    def get_latest_version(self, subject_name: str) -> RegisteredSchema: ...
    """
    Retrieves latest registered version for subject

    Args:
        subject_name (str): Subject name.

    Returns:
        RegisteredSchema: Registration information for this version.

    Raises:
        SchemaRegistryError: if the version can't be found or is invalid.

    See Also:
        `GET Subject Version API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#get--subjects-(string-%20subject)-versions-(versionId-%20version)>`_
    """  # noqa: E501


    def get_version(self, subject_name: str, version: int) -> RegisteredSchema: ...
    """
    Retrieves a specific schema registered under ``subject_name``.

    Args:
        subject_name (str): Subject name.

        version (int): version number. Defaults to latest version.

    Returns:
        RegisteredSchema: Registration information for this version.

    Raises:
        SchemaRegistryError: if the version can't be found or is invalid.

    See Also:
        `GET Subject Version API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#get--subjects-(string-%20subject)-versions-(versionId-%20version)>`_
    """  # noqa: E501


    def get_versions(self, subject_name: str) -> list[int]: ...
    """
    Get a list of all versions registered with this subject.

    Args:
        subject_name (str): Subject name.

    Returns:
        list(int): Registered versions

    Raises:
        SchemaRegistryError: If subject can't be found

    See Also:
        `GET Subject Versions API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#post--subjects-(string-%20subject)-versions>`_
    """  # noqa: E501


    def delete_version(self, subject_name: str, version: int) -> int: ...
    """
    Deletes a specific version registered to ``subject_name``.

    Args:
        subject_name (str) Subject name

        version (int): Version number

    Returns:
        int: Version number which was deleted

    Raises:
        SchemaRegistryError: if the subject or version cannot be found.

    See Also:
        `Delete Subject Version API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#delete--subjects-(string-%20subject)-versions-(versionId-%20version)>`_
    """  # noqa: E501


    def set_compatibility(self, subject_name: Optional[str], level: Optional[str]) -> str: ...
    """
    Update global or subject level compatibility level.

    Args:
        level (str): Compatibility level. See API reference for a list of
            valid values.

        subject_name (str, optional): Subject to update. Sets compatibility
            level policy if not set.

    Returns:
        str: The newly configured compatibility level.

    Raises:
        SchemaRegistryError: If the compatibility level is invalid.

    See Also:
        `PUT Subject Compatibility API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#put--config-(string-%20subject)>`_
    """  # noqa: E501


    def get_compatibility(self, subject_name: Optional[str]) -> str: ...
    """
    Get the current compatibility level.

    Args:
        subject_name (str, optional): Subject name. Returns global policy
            if left unset.

    Returns:
        str: Compatibility level for the subject if set, otherwise the global compatibility level.

    Raises:
        SchemaRegistryError: if the request was unsuccessful.

    See Also:
        `GET Subject Compatibility API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#get--config-(string-%20subject)>`_
    """  # noqa: E501


    def test_compatibility(self, subject_name: str, schema: str, version: Optional[Union[int, Literal["latest"]]]) -> bool: ...
    """Test the compatibility of a candidate schema for a given subject and version

    Args:
        subject_name (str): Subject name the schema is registered under

        schema (Schema): Schema instance.

        version (int or str, optional): Version number, or the string "latest". Defaults to "latest".

    Returns:
        bool: True if the schema is compatible with the specified version

    Raises:
        SchemaRegistryError: if the request was unsuccessful.

    See Also:
        `POST Test Compatibility API Reference <https://docs.confluent.io/current/schema-registry/develop/api.html#post--compatibility-subjects-(string-%20subject)-versions-(versionId-%20version)>`_
    """  # noqa: E501


class Schema(object):
    """
    An unregistered Schema.

    Args:
        schema_str (str): String representation of the schema.

        schema_type (str): The schema type: AVRO, PROTOBUF or JSON.

        references ([SchemaReference]): SchemaReferences used in this schema.
    """
    __slots__ = ['schema_str', 'schema_type', 'references', '_hash']

    schema_str: str
    schema_type: str
    references: list[SchemaReference]

    def __init__(self, schema_str: str, schema_type: str, references: list[SchemaReference]) -> None: ...


class RegisteredSchema(object):
    """
    Schema registration information.

    Represents a  Schema registered with a subject. Use this class when you need
    a specific version of a subject such as forming a SchemaReference.

    Args:
        schema_id (int): Registered Schema id

        schema (Schema): Registered Schema

        subject (str): Subject this schema is registered under

        version (int): Version of this subject this schema is registered to
    """

    schema_id: int
    schema: Schema
    subject: str
    version: int

    def __init__(self, schema_id: int, schema: Schema, subject: str, version: int) -> None: ...


class SchemaReference(object):
    """
    Reference to a Schema registered with the Schema Registry.

    As of Confluent Platform 5.5 Schema's may now hold references to other
    registered schemas. As long as there is a references to a schema it may not
    be deleted.

    Args:
        name (str): Schema name

        subject (str): Subject this Schema is registered with

        version (int): This Schema's version
    """

    name: str
    subject: str
    version: int

    def __init__(self, name: str, subject: str, version: int) -> None: ...
