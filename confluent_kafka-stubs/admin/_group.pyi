# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from confluent_kafka import KafkaException, TopicPartition
from ._acl import AclOperation
from .._model import ConsumerGroupState, Node

class MemberAssignment:
    """
    Represents member assignment information.
    Used by :class:`MemberDescription`.

    Parameters
    ----------
    topic_partitions : list(TopicPartition)
        The topic partitions assigned to a group member.
    """

    topic_partitions: list[TopicPartition]

class MemberDescription:
    """
    Represents member information.
    Used by :class:`ConsumerGroupDescription`.

    Parameters
    ----------
    member_id : str
        The consumer id of the group member.
    client_id : str
        The client id of the group member.
    host: str
        The host where the group member is running.
    assignment: MemberAssignment
        The assignment of the group member
    group_instance_id : str
        The instance id of the group member.
    """

    member_id: str
    client_id: str
    host: str
    assignment: MemberAssignment
    group_instance_id: str

class ConsumerGroupDescription:
    """
    Represents consumer group description information for a group used in describe consumer group operation.
    Used by :meth:`AdminClient.describe_consumer_groups`.

    Parameters
    ----------
    group_id : str
        The consumer group id.
    is_simple_consumer_group : bool
        Whether a consumer group is simple or not.
    members: list(MemberDescription)
        Description of the members of the consumer group.
    partition_assignor: str
        Partition assignor.
    state : ConsumerGroupState
        Current state of the consumer group.
    coordinator: Node
        Consumer group coordinator.
    authorized_operations: list(AclOperation)
        AclOperations allowed for the consumer group.
    """

    group_id: str
    is_simple_consumer_group: bool
    members: list[MemberDescription]
    partition_assignor: str
    state: ConsumerGroupState
    coordinator: Node
    authorized_operations: list[AclOperation]

class ConsumerGroupListing:
    """
    Represents consumer group listing information for a group used in list consumer group operation.
    Used by :class:`ListConsumerGroupsResult`.

    Parameters
    ----------
    group_id : str
        The consumer group id.
    is_simple_consumer_group : bool
        Whether a consumer group is simple or not.
    state : ConsumerGroupState
        Current state of the consumer group.
    """

    group_id: str
    is_simple_consumer_group: bool
    members: list[MemberDescription]
    partition_assignor: str
    state: ConsumerGroupState
    coordinator: Node
    authorized_operations: list[AclOperation]

class ListConsumerGroupsResult:
    """
    Represents result of List Consumer Group operation.
    Used by :meth:`AdminClient.list_consumer_groups`.

    Parameters
    ----------
    valid : list(ConsumerGroupListing)
        List of successful consumer group listing responses.
    errors : list(KafkaException)
        List of errors encountered during the operation, if any.
    """

    valid: list[ConsumerGroupListing]
    errors: list[KafkaException]
