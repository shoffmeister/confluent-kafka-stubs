# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from enum import Enum

class ResourceType(Enum):
    """
    Enumerates the different types of Kafka resources.
    """

    UNKNOWN: int  #: Resource type is not known or not set.
    ANY: int  #: Match any resource, used for lookups.
    TOPIC: int  #: Topic resource. Resource name is topic name.
    GROUP: int  #: Group resource. Resource name is group.id.
    BROKER: int  #: Broker resource. Resource name is broker id.

class ResourcePatternType(Enum):
    """
    Enumerates the different types of Kafka resource patterns.
    """

    UNKNOWN: int  #: Resource pattern type is not known or not set.
    ANY: int  #: Match any resource, used for lookups.
    MATCH: int  #: Match: will perform pattern matching
    LITERAL: int  #: Literal: A literal resource name
    PREFIXED: int  #: Prefixed: A prefixed resource name
