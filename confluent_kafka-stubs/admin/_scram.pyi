# Copyright 2023 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from enum import Enum
from typing import Optional
from .. import cimpl

class ScramMechanism(Enum):
    """
    Enumerates SASL/SCRAM mechanisms.
    """
    UNKNOWN = cimpl.SCRAM_MECHANISM_UNKNOWN  #: Unknown SASL/SCRAM mechanism
    SCRAM_SHA_256 = cimpl.SCRAM_MECHANISM_SHA_256  #: SCRAM-SHA-256 mechanism
    SCRAM_SHA_512 = cimpl.SCRAM_MECHANISM_SHA_512  #: SCRAM-SHA-512 mechanism

class ScramCredentialInfo:
    """
    Contains mechanism and iterations for a
    SASL/SCRAM credential associated with a user.

    Parameters
    ----------
    mechanism: ScramMechanism
        SASL/SCRAM mechanism.
    iterations: int
        Positive number of iterations used when creating the credential.
    """
    mechanism: ScramMechanism
    iterations: int


class UserScramCredentialsDescription:
    """
    Represent all SASL/SCRAM credentials
    associated with a user that can be retrieved.

    Parameters
    ----------
    user: str
        The user name.
    scram_credential_infos: list(ScramCredentialInfo)
        SASL/SCRAM credential representations for the user.
    """
    user: str
    scram_credential_infos: list[ScramCredentialInfo]

class UserScramCredentialAlteration:
    """
    Base class for SCRAM credential alterations.

    Parameters
    ----------
    user: str
        The user name.
    """
    user: str


class UserScramCredentialUpsertion(UserScramCredentialAlteration):
    """
    A request to update/insert a SASL/SCRAM credential for a user.

    Parameters
    ----------
    user: str
        The user name.
    scram_credential_info: ScramCredentialInfo
        The mechanism and iterations.
    password: bytes
        Password to HMAC before storage.
    salt: bytes
        Salt to use. Will be generated randomly if None. (optional)
    """
    user: str
    scram_credential_info: ScramCredentialInfo
    password: bytes
    salt: bytes

    def __init__(self, user: str, scram_credential_info: ScramCredentialInfo, password: bytes, salt: Optional[bytes]) -> None: ...


class UserScramCredentialDeletion(UserScramCredentialAlteration):
    """
    A request to delete a SASL/SCRAM credential for a user.

    Parameters
    ----------
    user: str
        The user name.
    mechanism: ScramMechanism
        SASL/SCRAM mechanism.
    """
    user: str
    mechanism: ScramMechanism
    def __init__(self, user: str, mechanism: ScramMechanism) -> None: ...
