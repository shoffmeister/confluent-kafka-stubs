# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Kafka admin client: create, view, alter, and delete topics and resources.
"""

from concurrent.futures import Future
from typing import Optional, TypedDict, Union
from typing_extensions import NotRequired, Unpack
from ..cimpl import ConfigDict, IsolationLevel, TopicPartition
from .._model import ConsumerGroupTopicPartitions
from ._acl import AclBinding, AclBindingFilter
from ._cluster import DescribeClusterResult
from ._config import ConfigEntry, ConfigResource
from ._group import ConsumerGroupDescription
from ._metadata import ClusterMetadata, GroupMetadata
from ._scram import UserScramCredentialAlteration, UserScramCredentialsDescription
from ._topic import TopicDescription

# dedicated line to disable typing warnings
from ..cimpl import _AdminClientImpl  # type: ignore

from ._listoffsets import ListOffsetsResultInfo, OffsetSpec
from ._group import ListConsumerGroupsResult
from .._model import ConsumerGroupState

TopicConfig = dict[str, str]

class AdminClientCreateTopicsKwargs(TypedDict):
    """
    kwargs for AdminClient.create_topics
    """

    operation_timeout: NotRequired[float]
    request_timeout: NotRequired[float]
    validate_only: NotRequired[bool]

class AdminClientDeleteTopicsKwargs(TypedDict):
    """
    kwargs for AdminClient.delete_topics
    """

    operation_timeout: NotRequired[float]
    request_timeout: NotRequired[float]

class AdminClientListTopicsKwargs(TypedDict):
    """
    kwargs for AdminClient.list_topics
    """

    timeout: NotRequired[float]

class AdminClientListGroupsKwargs(TypedDict):
    """
    kwargs for AdminClient.list_groups
    """

    timeout: NotRequired[float]

class AdminClientCreatePartitionsKwargs(TypedDict):
    """
    kwargs for AdminClient.create_partitions
    """

    operation_timeout: NotRequired[float]
    request_timeout: NotRequired[float]
    validate_only: NotRequired[bool]

class AdminClientDescribeConfigsKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_configs
    """

    request_timeout: NotRequired[float]

class AdminClientAlterConfigsKwargs(TypedDict):
    """
    kwargs for AdminClient.alter_configs
    """

    request_timeout: NotRequired[float]
    validate_only: NotRequired[bool]

class AdminClientIncrementalAlterConfigsKwargs(TypedDict):
    """
    kwargs for AdminClient.incremental_alter_configs
    """
    request_timeout: NotRequired[float]
    validate_only: NotRequired[bool]
    broker: NotRequired[int]

class AdminClientCreateAclsKwargs(TypedDict):
    """
    kwargs for AdminClient.create_acls
    """
    request_timeout: NotRequired[float]

class AdminClientDescribeAclsKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_acls
    """
    request_timeout: NotRequired[float]


class AdminClientDeleteAclsKwargs(TypedDict):
    """
    kwargs for AdminClient.delete_acls
    """
    request_timeout: NotRequired[float]


class AdminClientDeleteConsumerGroupsKwargs(TypedDict):
    """
    kwargs for AdminClient.delete_consumer_groups
    """

    request_timeout: NotRequired[float]

class AdminClientListConsumerGroupOffsetsKwargs(TypedDict):
    """
    kwargs for AdminClient.list_consumer_group_offsets
    """
    require_stable: NotRequired[bool]
    request_timeout: NotRequired[float]

class AdminClientAlterConsumerGroupOffsetsKwargs(TypedDict):
    """
    kwargs for AdminClient.alter_consumer_group_offsets
    """
    request_timeout: NotRequired[float]

class AdminClientDescribeUserScramCredentialsKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_user_scram_credentials
    """
    request_timeout: NotRequired[float]

class AdminClientAlterUserScramCredentialsKwargs(TypedDict):
    """
    kwargs for AdminClient.alter_user_scram_credentials
    """
    request_timeout: NotRequired[float]

class AdminClientListOffsetsKwargs(TypedDict):
    """
    kwargs for AdminClient.list_offsets
    """

    request_timeout: NotRequired[float]
    isolation_level: NotRequired[IsolationLevel]

class AdminClientListConsumerGroupsKwargs(TypedDict):
    """
    kwargs for AdminClient.list_consumer_groups
    """

    request_timeout: NotRequired[float]
    states: NotRequired[set[ConsumerGroupState]]


class AdminClientDescribeConsumerGroupsKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_consumer_groups
    """
    include_authorized_operations: NotRequired[bool]
    request_timeout: NotRequired[float]

class AdminClientDescribeTopicsKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_topics
    """
    include_authorized_operations: NotRequired[bool]
    request_timeout: NotRequired[float]

class AdminClientDescribeClusterKwargs(TypedDict):
    """
    kwargs for AdminClient.describe_cluster
    """

    include_authorized_operations: NotRequired[bool]
    request_timeout: NotRequired[float]

class AdminClient(_AdminClientImpl):
    """
    AdminClient provides admin operations for Kafka brokers, topics, groups,
    and other resource types supported by the broker.

    The Admin API methods are asynchronous and return a dict of
    concurrent.futures.Future objects keyed by the entity.
    The entity is a topic name for create_topics(), delete_topics(), create_partitions(),
    and a ConfigResource for alter_configs() and describe_configs().

    All the futures for a single API call will currently finish/fail at
    the same time (backed by the same protocol request), but this might
    change in future versions of the client.

    See examples/adminapi.py for example usage.

    For more information see the `Java Admin API documentation
    <https://docs.confluent.io/current/clients/javadocs/org/apache/kafka/clients/admin/package-frame.html>`_.

    Requires broker version v0.11.0.0 or later.
    """

    def __init__(self, config: ConfigDict) -> None: ...
    """
    Create a new AdminClient using the provided configuration dictionary.

    The AdminClient is a standard Kafka protocol client, supporting
    the standard librdkafka configuration properties as specified at
    https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md

    At least 'bootstrap.servers' should be configured.
    """
    def create_topics(
        self,
        new_topics: list[NewTopic],
        **kwargs: Unpack[AdminClientCreateTopicsKwargs],
    ) -> dict[str, Future[None]]: ...
    """
    Create one or more new topics.

    :param list(NewTopic) new_topics: A list of specifictions (NewTopic) for
                the topics that should be created.
    :param float operation_timeout: The operation timeout in seconds,
                controlling how long the CreateTopics request will block
                on the broker waiting for the topic creation to propagate
                in the cluster. A value of 0 returns immediately. Default: 0
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`
    :param bool validate_only: If true, the request is only validated
                without creating the topic. Default: False

    :returns: A dict of futures for each topic, keyed by the topic name.
                The future result() method returns None.

    :rtype: dict(<topic_name, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def delete_topics(
        self, topics: list[str], **kwargs: Unpack[AdminClientDeleteTopicsKwargs]
    ) -> dict[str, Future[None]]: ...
    """
    Delete one or more topics.

    :param list(str) topics: A list of topics to mark for deletion.
    :param float operation_timeout: The operation timeout in seconds,
                controlling how long the DeleteTopics request will block
                on the broker waiting for the topic deletion to propagate
                in the cluster. A value of 0 returns immediately. Default: 0
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each topic, keyed by the topic name.
                The future result() method returns None.

    :rtype: dict(<topic_name, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def list_topics(
        self, topic: Optional[str] = None, **kwargs: Unpack[AdminClientListTopicsKwargs]
    ) -> ClusterMetadata: ...
    def list_groups(
        self, group: Optional[str], **kwargs: Unpack[AdminClientListGroupsKwargs]
    ) -> GroupMetadata: ...
    """
    Deprecated since version 2.0.2: Use list_consumer_groups() and describe_consumer_groups instead.

    Request Group Metadata from cluster. This method provides the same information as listGroups(), describeGroups() in the Java Admin client.

    :param str group: If specified, only request info about this group, else return for all groups in cluster 
    :param float timeout: Maximum response time before timing out, or -1 for infinite timeout.

    :rtype: GroupMetadata

    :raises: KafkaException
    """
    def create_partitions(
        self,
        new_partitions: list[NewPartitions],
        **kwargs: Unpack[AdminClientCreatePartitionsKwargs],
    ) -> dict[str, Future[None]]: ...
    """
    Create additional partitions for the given topics.

    :param list(NewPartitions) new_partitions: New partitions to be created.
    :param float operation_timeout: The operation timeout in seconds,
                controlling how long the CreatePartitions request will block
                on the broker waiting for the partition creation to propagate
                in the cluster. A value of 0 returns immediately. Default: 0
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`
    :param bool validate_only: If true, the request is only validated
                without creating the partitions. Default: False

    :returns: A dict of futures for each topic, keyed by the topic name.
                The future result() method returns None.

    :rtype: dict(<topic_name, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def describe_configs(
        self,
        resources: list[ConfigResource],
        **kwargs: Unpack[AdminClientDescribeConfigsKwargs],
    ) -> dict[ConfigResource, Future[dict[str, ConfigEntry]]]: ...
    """
    Get the configuration of the specified resources.

    :warning: Multiple resources and resource types may be requested,
                but at most one resource of type RESOURCE_BROKER is allowed
                per call since these resource requests must be sent to the
                broker specified in the resource.

    :param list(ConfigResource) resources: Resources to get the configuration for.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each resource, keyed by the ConfigResource.
                The type of the value returned by the future result() method is
                dict(<configname, ConfigEntry>).

    :rtype: dict(<ConfigResource, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def alter_configs(
        self,
        resources: list[ConfigResource],
        **kwargs: Unpack[AdminClientAlterConfigsKwargs],
    ) -> dict[ConfigResource, Future[dict[str, ConfigEntry]]]: ...
    """
    .. deprecated:: 2.2.0

    Update configuration properties for the specified resources.
    Updates are not transactional so they may succeed for a subset
    of the provided resources while the others fail.
    The configuration for a particular resource is updated atomically,
    replacing the specified values while reverting unspecified configuration
    entries to their default values.

    :warning: alter_configs() will replace all existing configuration for
                the provided resources with the new configuration given,
                reverting all other configuration for the resource back
                to their default values.

    :warning: Multiple resources and resource types may be specified,
                but at most one resource of type RESOURCE_BROKER is allowed
                per call since these resource requests must be sent to the
                broker specified in the resource.

    :param list(ConfigResource) resources: Resources to update configuration of.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`.
    :param bool validate_only: If true, the request is validated only,
                without altering the configuration. Default: False

    :returns: A dict of futures for each resource, keyed by the ConfigResource.
                The future result() method returns None or throws a KafkaException.

    :rtype: dict(<ConfigResource, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeError: Invalid type.
    :raises ValueError: Invalid value.
    """
    def incremental_alter_configs(
        self,
        resources: list[ConfigResource],
        **kwargs: Unpack[AdminClientIncrementalAlterConfigsKwargs],
    ) -> dict[ConfigResource, Future[dict[str, ConfigEntry]]]: ...
    """
    Update configuration properties for the specified resources.
    Updates are incremental, i.e only the values mentioned are changed
    and rest remain as is.

    :param list(ConfigResource) resources: Resources to update configuration of.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`.
    :param bool validate_only: If true, the request is validated only,
                without altering the configuration. Default: False
    :param int broker: Broker id to send the request to. When
                altering broker configurations, it's ignored because
                the request needs to go to that broker only.
                Default: controller broker.

    :returns: A dict of futures for each resource, keyed by the ConfigResource.
                The future result() method returns None or throws a KafkaException.

    :rtype: dict(<ConfigResource, future>)

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeError: Invalid type.
    :raises ValueError: Invalid value.
    """
    def create_acls(
        self, acls: list[AclBinding], **kwargs: Unpack[AdminClientCreateAclsKwargs]
    ) -> dict[AclBinding, Future[None]]: ...
    """
    Create one or more ACL bindings.

    :param list(AclBinding) acls: A list of unique ACL binding specifications (:class:`.AclBinding`)
                        to create.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each ACL binding, keyed by the :class:`AclBinding` object.
                The future result() method returns None on success.

    :rtype: dict[AclBinding, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """

    def describe_acls(
        self,
        acl_binding_filter: AclBindingFilter,
        **kwargs: Unpack[AdminClientDescribeAclsKwargs],
    ) -> Future[list[AclBinding]]: ...
    """
    Match ACL bindings by filter.

    :param AclBindingFilter acl_binding_filter: a filter with attributes that
                must match.
                String attributes match exact values or any string if set to None.
                Enums attributes match exact values or any value if equal to `ANY`.
                If :class:`ResourcePatternType` is set to :attr:`ResourcePatternType.MATCH`
                returns ACL bindings with:
                :attr:`ResourcePatternType.LITERAL` pattern type with resource name equal
                to the given resource name;
                :attr:`ResourcePatternType.LITERAL` pattern type with wildcard resource name
                that matches the given resource name;
                :attr:`ResourcePatternType.PREFIXED` pattern type with resource name
                that is a prefix of the given resource name
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A future returning a list(:class:`AclBinding`) as result

    :rtype: future

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def delete_acls(
        self,
        acl_binding_filters: list[AclBindingFilter],
        **kwargs: Unpack[AdminClientDeleteAclsKwargs],
    ) -> dict[AclBindingFilter, Future[list[AclBinding]]]: ...
    """
    Delete ACL bindings matching one or more ACL binding filters.

    :param list(AclBindingFilter) acl_binding_filters: a list of unique ACL binding filters
                to match ACLs to delete.
                String attributes match exact values or any string if set to None.
                Enums attributes match exact values or any value if equal to `ANY`.
                If :class:`ResourcePatternType` is set to :attr:`ResourcePatternType.MATCH`
                deletes ACL bindings with:
                :attr:`ResourcePatternType.LITERAL` pattern type with resource name
                equal to the given resource name;
                :attr:`ResourcePatternType.LITERAL` pattern type with wildcard resource name
                that matches the given resource name;
                :attr:`ResourcePatternType.PREFIXED` pattern type with resource name
                that is a prefix of the given resource name
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each ACL binding filter, keyed by the :class:`AclBindingFilter` object.
                The future result() method returns a list of :class:`AclBinding`.

    :rtype: dict[AclBindingFilter, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def list_consumer_groups(
        self, **kwargs: Unpack[AdminClientListConsumerGroupsKwargs]
    ) -> Future[ListConsumerGroupsResult]: ...
    """
    List consumer groups.

    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`
    :param set(ConsumerGroupState) states: only list consumer groups which are currently in
                these states.

    :returns: a future. Result method of the future returns :class:`ListConsumerGroupsResult`.

    :rtype: future

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def describe_consumer_groups(self, group_ids: list[str], **kwargs: Unpack[AdminClientDescribeConsumerGroupsKwargs]) -> dict[str, Future[ConsumerGroupDescription]]: ...
    """
    Describe consumer groups.

    :param list(str) group_ids: List of group_ids which need to be described.
    :param bool include_authorized_operations: If True, fetches group AclOperations. Default: False
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each group, keyed by the group_id.
                The future result() method returns :class:`ConsumerGroupDescription`.

    :rtype: dict[str, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def describe_topics(self, topics: list[str], **kwargs: Unpack[AdminClientDescribeTopicsKwargs]) -> dict[str, Future[TopicDescription]]: ...
    """
    Describe topics.

    :param TopicCollection topics: Collection of list of topic names to describe.
    :param bool include_authorized_operations: If True, fetches topic AclOperations. Default: False
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each topic, keyed by the topic.
                The future result() method returns :class:`TopicDescription`.

    :rtype: dict[str, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """
    def describe_cluster(
        self, **kwargs: Unpack[AdminClientDescribeClusterKwargs]
    ) -> Future[DescribeClusterResult]: ...
    """
    Describe cluster.

    :param bool include_authorized_operations: If True, fetches topic AclOperations. Default: False
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A future returning description of the cluster as result

    :rtype: future containing the description of the cluster in result.

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """
    def delete_consumer_groups(
        self,
        group_ids: list[str],
        **kwargs: Unpack[AdminClientDeleteConsumerGroupsKwargs],
    ) -> dict[str, Future[None]]: ...
    """
    Delete the given consumer groups.

    :param list(str) group_ids: List of group_ids which need to be deleted.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each group, keyed by the group_id.
                The future result() method returns None.

    :rtype: dict[str, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """
    def list_consumer_group_offsets(
        self, list_consumer_group_offsets_request: list[ConsumerGroupTopicPartitions], **kwargs: Unpack[AdminClientListConsumerGroupOffsetsKwargs]
    ) -> dict[str, Future[ConsumerGroupTopicPartitions]]: ...
    """
    List offset information for the consumer group and (optional) topic partition provided in the request.

    :note: Currently, the API supports only a single group.

    :param list(ConsumerGroupTopicPartitions) list_consumer_group_offsets_request: List of
                :class:`ConsumerGroupTopicPartitions` which consist of group name and topic
                partition information for which offset detail is expected. If only group name is
                provided, then offset information of all the topic and partition associated with
                that group is returned.
    :param bool require_stable: If True, fetches stable offsets. Default: False
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each group, keyed by the group id.
                The future result() method returns :class:`ConsumerGroupTopicPartitions`.

    :rtype: dict[str, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def alter_consumer_group_offsets(
        self, alter_consumer_group_offsets_request: list[ConsumerGroupTopicPartitions], **kwargs: Unpack[AdminClientAlterConsumerGroupOffsetsKwargs]
    ) -> dict[str, Future[ConsumerGroupTopicPartitions]]: ...
    """
    Alter offset for the consumer group and topic partition provided in the request.

    :note: Currently, the API supports only a single group.

    :param list(ConsumerGroupTopicPartitions) alter_consumer_group_offsets_request: List of
                :class:`ConsumerGroupTopicPartitions` which consist of group name and topic
                partition; and corresponding offset to be updated.
    :param float request_timeout: The overall request timeout in seconds,
                including broker lookup, request transmission, operation time
                on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures for each group, keyed by the group id.
                The future result() method returns :class:`ConsumerGroupTopicPartitions`.

    :rtype: dict[ConsumerGroupTopicPartitions, future]

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    :raises ValueException: Invalid input.
    """
    def set_sasl_credentials(self, username: str, password: str) -> None: ...
    """
    Sets the SASL credentials used for this client.
    These credentials will overwrite the old ones, and will be used the
    next time the client needs to authenticate.
    This method will not disconnect existing broker connections that
    have been established with the old credentials.
    This method is applicable only to SASL PLAIN and SCRAM mechanisms.

    :param str username: The username to set.
    :param str password: The password to set.

    :rtype: None

    :raises KafkaException: Operation failed locally or on broker.
    :raises TypeException: Invalid input.
    """
    def describe_user_scram_credentials(
        self, users: Optional[list[str]], **kwargs: Unpack[AdminClientDescribeUserScramCredentialsKwargs]
    ) -> Union[Future[dict[str, UserScramCredentialsDescription]],
                    dict[str, Future[UserScramCredentialsDescription]]]: ...
    """
    Describe user SASL/SCRAM credentials.

    :param list(str) users: List of user names to describe.
            Duplicate users aren't allowed. Can be None
            to describe all user's credentials.
    :param float request_timeout: The overall request timeout in seconds,
            including broker lookup, request transmission, operation time
            on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: In case None is passed it returns a single future.
                The future yields a dict[str, UserScramCredentialsDescription]
                or raises a KafkaException

                In case a list of user names is passed, it returns
                a dict[str, future[UserScramCredentialsDescription]].
                The futures yield a :class:`UserScramCredentialsDescription`
                or raise a KafkaException

    :rtype: Union[future[dict[str, UserScramCredentialsDescription]],
                    dict[str, future[UserScramCredentialsDescription]]]

    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """
    def alter_user_scram_credentials(self, alterations: list[UserScramCredentialAlteration], **kwargs: Unpack[AdminClientAlterUserScramCredentialsKwargs]) -> dict[str, Future[None]]: ...
    """
    Alter user SASL/SCRAM credentials.

    :param list(UserScramCredentialAlteration) alterations: List of
            :class:`UserScramCredentialAlteration` to apply.
            The pair (user, mechanism) must be unique among alterations.
    :param float request_timeout: The overall request timeout in seconds,
            including broker lookup, request transmission, operation time
            on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures keyed by user name.
                The future result() method returns None or
                raises KafkaException

    :rtype: dict[str, future]

    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """
    def list_offsets(
        self,
        topic_partition_offsets: dict[TopicPartition, OffsetSpec],
        **kwargs: AdminClientListOffsetsKwargs,
    ) -> dict[TopicPartition, Future[ListOffsetsResultInfo]]: ...
    """
    Enables to find the beginning offset,
    end offset as well as the offset matching a timestamp
    or the offset with max timestamp in partitions.

    :param dict([TopicPartition, OffsetSpec]) topic_partition_offsets: Dictionary of
            TopicPartition objects associated with the corresponding OffsetSpec to query for.
    :param IsolationLevel isolation_level: The isolation level to use when
            querying.
    :param float request_timeout: The overall request timeout in seconds,
            including broker lookup, request transmission, operation time
            on broker, and response. Default: `socket.timeout.ms*1000.0`

    :returns: A dict of futures keyed by TopicPartition.
                The future result() method returns ListOffsetsResultInfo
                raises KafkaException

    :rtype: dict[TopicPartition, future]

    :raises TypeError: Invalid input type.
    :raises ValueError: Invalid input value.
    """

class NewTopic:
    def __init__(
        self,
        topic: str,
        num_partitions: Optional[int] = None,
        replication_factor: Optional[int] = None,
        replica_assignment: Optional[list[list[int]]] = None,
        config: Optional[TopicConfig] = None,
    ) -> None: ...

class NewPartitions:
    def __init__(
        self,
        topic: str,
        new_total_count: int,
        replica_assignment: Optional[list[list[str]]] = None,
    ) -> None: ...
