# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from enum import Enum
from ._resource import ResourceType, ResourcePatternType

class AclOperation(Enum):
    """
    Enumerates the different types of ACL operation.
    """

    UNKNOWN: int  #: Unknown
    ANY: int  #: In a filter, matches any AclOperation
    ALL: int  #: ALL the operations
    READ: int  #: READ operation
    WRITE: int  #: WRITE operation
    CREATE: int  #: CREATE operation
    DELETE: int  #: DELETE operation
    ALTER: int  #: ALTER operation
    DESCRIBE: int  #: DESCRIBE operation
    CLUSTER_ACTION: int  #: CLUSTER_ACTION operation
    DESCRIBE_CONFIGS: int  #: DESCRIBE_CONFIGS operation
    ALTER_CONFIGS: int  #: ALTER_CONFIGS operation
    IDEMPOTENT_WRITE: int  #: IDEMPOTENT_WRITE operation

class AclPermissionType(Enum):
    """
    Enumerates the different types of ACL permission types.
    """

    UNKNOWN: int  #: Unknown
    ANY: int  #: In a filter, matches any AclPermissionType
    DENY: int  #: Disallows access
    ALLOW: int  #: Grants access

class AclBinding(object):
    """
    Represents an ACL binding that specify the operation and permission type for a specific principal
    over one or more resources of the same type. Used by :meth:`AdminClient.create_acls`,
    returned by :meth:`AdminClient.describe_acls` and :meth:`AdminClient.delete_acls`.

    Parameters
    ----------
    restype : ResourceType
        The resource type.
    name : str
        The resource name, which depends on the resource type. For :attr:`ResourceType.BROKER`,
        the resource name is the broker id.
    resource_pattern_type : ResourcePatternType
        The resource pattern, relative to the name.
    principal : str
        The principal this AclBinding refers to.
    host : str
        The host that the call is allowed to come from.
    operation: AclOperation
        The operation/s specified by this binding.
    permission_type: AclPermissionType
        The permission type for the specified operation.
    """

    restype: ResourceType
    name: str
    resource_pattern_type: ResourcePatternType
    principal: str
    host: str
    operation: AclOperation
    permission_type: AclPermissionType

    def __init__(
        self,
        restype: ResourceType,
        name: str,
        resource_pattern_type: ResourcePatternType,
        principal: str,
        host: str,
        operation: AclOperation,
        permission_type: AclPermissionType,
    ) -> None: ...

class AclBindingFilter(AclBinding):
    """
    Represents an ACL binding filter used to return a list of ACL bindings matching some or all of its attributes.
    Used by :meth:`AdminClient.describe_acls` and :meth:`AdminClient.delete_acls`.

    Parameters
    ----------
    restype : ResourceType
        The resource type, or :attr:`ResourceType.ANY` to match any value.
    name : str
        The resource name to match.
        None matches any value.
    resource_pattern_type : ResourcePatternType
        The resource pattern, :attr:`ResourcePatternType.ANY` to match any value or
        :attr:`ResourcePatternType.MATCH` to perform pattern matching.
    principal : str
        The principal to match, or None to match any value.
    host : str
        The host to match, or None to match any value.
    operation: AclOperation
        The operation to match or :attr:`AclOperation.ANY` to match any value.
    permission_type: AclPermissionType
        The permission type to match or :attr:`AclPermissionType.ANY` to match any value.
    """
