# Copyright 2022 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from typing import Any, Optional

from confluent_kafka._model import ConsumerGroupState
from confluent_kafka.cimpl import KafkaError

class ClusterMetadata:
    """
    Provides information about the Kafka cluster, brokers, and topics.
    Returned by list_topics().

    This class is typically not user instantiated.
    """
    cluster_id: Optional[str]
    """Cluster id string, if supported by the broker, else None."""
    controller_id: int
    """Current controller broker id, or -1."""
    brokers: dict[int, BrokerMetaData]
    """Map of brokers indexed by the broker id (int). Value is a BrokerMetadata object."""
    topics: dict[str, TopicMetadata]
    """Map of topics indexed by the topic name. Value is a TopicMetadata object."""
    orig_broker_id: int
    """The broker this metadata originated from."""
    orig_broker_name: Optional[str]
    """The broker name/address this metadata originated from."""

class BrokerMetaData:
    """
    Provides information about a Kafka broker.

    This class is typically not user instantiated.
    """
    id: int
    """Broker id"""
    host: Optional[str]
    """Broker hostname"""
    port: int
    """Broker port"""

class TopicMetadata:
    """
    Provides information about a Kafka topic.

    This class is typically not user instantiated.
    """
    topic: Optional[str]
    """Topic name"""
    partitions: dict[int, PartitionMetadata]
    """Map of partitions indexed by partition id. Value is a PartitionMetadata object."""
    error: Optional[KafkaError]
    """Topic error, or None. Value is a KafkaError object."""

class PartitionMetadata:
    """
    Provides information about a Kafka partition.

    This class is typically not user instantiated.

    :warning: Depending on cluster state the broker ids referenced in
              leader, replicas and ISRs may temporarily not be reported
              in ClusterMetadata.brokers. Always check the availability
              of a broker id in the brokers dict.
    """
    id: int
    """Partition id."""
    leader: int
    """Current leader broker for this partition, or -1."""
    replicas: list[int]
    """List of replica broker ids for this partition."""
    isrs: list[int]
    """List of in-sync-replica broker ids for this partition."""
    error: Optional[KafkaError]
    """Partition error, or None. Value is a KafkaError object."""

class GroupMember:
    """Provides information about a group member.

    For more information on the metadata format, refer to:
    `A Guide To The Kafka Protocol <https://cwiki.apache.org/confluence/display/KAFKA/A+Guide+To+The+Kafka+Protocol#AGuideToTheKafkaProtocol-GroupMembershipAPI>`_.

    This class is typically not user instantiated.
    """
    id: Optional[int]
    """Member id (generated by broker)."""
    client_id: Optional[str]
    """Client id."""
    client_host: Optional[str]
    """Client hostname."""
    metadata: Optional[bytes]
    """Member metadata(binary), format depends on protocol type."""
    assignment: Optional[bytes]
    """Member assignment(binary), format depends on protocol type."""


class GroupMetadata(object):
    """GroupMetadata provides information about a Kafka consumer group

    This class is typically not user instantiated.
    """

    broker: Optional[BrokerMetaData]
    """Originating broker metadata."""
    id: Optional[str]
    """Group name."""
    error = Optional[KafkaError]
    """Broker-originated error, or None. Value is a KafkaError object."""
    state: ConsumerGroupState
    """Group state."""
    protocol_type: Any
    """Group protocol type."""
    protocol: Any
    """Group protocol."""
    members = list[GroupMember]
    """Group members."""
