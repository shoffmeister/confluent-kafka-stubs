# Copyright 2023 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import ABC


class OffsetSpec(ABC):

    @classmethod
    def earliest(cls) -> "EarliestSpec": ...

    @classmethod
    def latest(cls) -> "LatestSpec": ...

    @classmethod
    def max_timestamp(cls) -> "MaxTimestampSpec": ...

    @classmethod
    def for_timestamp(cls, timestamp: int) -> "TimestampSpec": ...


class TimestampSpec(OffsetSpec):
    pass

class MaxTimestampSpec(OffsetSpec):
    pass

class LatestSpec(OffsetSpec):
    pass

class EarliestSpec(OffsetSpec):
    pass

class ListOffsetsResultInfo:
    offset: int
    timestamp: int
    leader_epoch: int
