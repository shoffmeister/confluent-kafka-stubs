#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2020 Confluent Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from confluent_kafka.cimpl import KafkaException
from confluent_kafka.serialization import SerializationError

class _KafkaClientError(KafkaException):
    """
    Wraps all errors encountered by a Kafka Client

    Args:
        kafka_error (KafkaError): KafkaError instance.

        exception(Exception, optional): The original exception

        kafka_message (Message, optional): The Kafka Message returned
        by the broker.
    """

    @property
    def code(self) -> int: ...
    @property
    def name(self) -> str: ...

class ConsumeError(_KafkaClientError):
    """
    Wraps all errors encountered during the consumption of a message.

    Note:
        In the event of a serialization error the original message
        contents may be retrieved from the ``kafka_message`` attribute.

    Args:
        kafka_error (KafkaError): KafkaError instance.

        exception(Exception, optional): The original exception

        kafka_message (Message, optional): The Kafka Message
        returned by the broker.

    """

class KeyDeserializationError(ConsumeError, SerializationError):
    """
    Wraps all errors encountered during the deserialization of a Kafka
    Message's key.

    Args:
        exception(Exception, optional): The original exception

        kafka_message (Message, optional): The Kafka Message returned
        by the broker.

    """

class ValueDeserializationError(ConsumeError, SerializationError):
    """
    Wraps all errors encountered during the deserialization of a Kafka
    Message's value.

    Args:
        exception(Exception, optional): The original exception

        kafka_message (Message, optional): The Kafka Message returned
        by the broker.

    """

class ProduceError(_KafkaClientError):
    """
    Wraps all errors encountered when Producing messages.

    Args:
        kafka_error (KafkaError): KafkaError instance.

        exception(Exception, optional): The original exception.
    """

class KeySerializationError(ProduceError, SerializationError):
    """
    Wraps all errors encountered during the serialization of a Message key.

    Args:
        exception (Exception): The exception that occurred during serialization.
    """

class ValueSerializationError(ProduceError, SerializationError):
    """
    Wraps all errors encountered during the serialization of a Message value.

    Args:
        exception (Exception): The exception that occurred during serialization.
    """
